///Опишите своими словами разницу между функциями setTimeout() и setInterval().
//setTimeout() начинает свой отсчет только после того как предыдущая итерация выполнена, а setInterval() начинает свой
//отсчет сразу после старта итерации. Если на выполнение итерации будет затрачиваться разное количество времени, то время
//между итерациями будет разное. setTimeout() же ждет когда итерация закончится и только тогда начинает отсчет, позволяя
// таким образом всегда получать одинаковое время между итерациями


// Что произойдет, если в функцию setTimeout() передать нулевую задержку? Сработает ли она мгновенно, и почему?
// сначала выполняется основной код, считываются переменные. Нулевая задержка не сработает мгновенно, функция
//setTimeout() выполнится после обработки остального кода


// Почему важно не забывать вызывать функцию clearInterval(), когда ранее созданный цикл запуска вам уже не нужен?
// чтобы остановать выполнение функции setInterval() если она нам уже не нужна


let images = new Array();
let currentValue = 0;
let timer = 0


images[0] = './img/1.jpg';
images[1] = './img/2.jpg';
images[2] = './img/3.jpg';
images[3] = './img/4.png';

function viewImages() {
    timer = setInterval(() => {
        document.getElementById('img-to-show').src = images[currentValue];
        currentValue += 1;
        fadeIn('.main-img');
        if (currentValue == images.length) {
            currentValue = 0;
        }
    }, 3000);


    const stop = document.getElementById("stop");
    stop.addEventListener("click", function () {
        clearInterval(timer);
        timer = null;
    });


    const resume = document.getElementById("resume")
    resume.addEventListener("click", function f() {
        if (!timer) viewImages();

    })
}


viewImages();

function fadeIn(el) {

    let opacity = 0.01;

    document.querySelector(el).style.display = "block";

    let timer = setInterval(function() {

        if(opacity >= 1) {

            clearInterval(timer);

        }

        document.querySelector(el).style.opacity = opacity;

        opacity += opacity * 0.1;

    }, 10);

}







